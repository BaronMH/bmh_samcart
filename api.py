from flask import Flask, jsonify, abort, request
from impl import car_api_implementation

app = Flask(__name__)


@app.route('/api/v0.1/car', methods=['GET'])
def get_cars():
    data = car_api_implementation.get_car_json_from_data_file()
    return jsonify(data)


@app.route('/api/v0.1/car/<string:car_id>', methods=['GET'])
def get_cars_from_id(car_id):
    data = car_api_implementation.get_car_json_from_id(car_id)

    return jsonify(data)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=105)
