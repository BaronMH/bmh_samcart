import unittest
import os
import collections
from impl import car_api_implementation


class DataFileTest(unittest.TestCase):
    def test_data_file(self):
        path = '/data/data.csv'
        self.assertTrue(path, "File does not exist")

    def test_data_file_json(self):
        mock_json = {'Make': 'Ford', 'Model': 'F10', 'Package': 'Base', 'Color': 'Silver', 'Year': '2010',
                     'Category': 'Truck', 'Mileage(mi)': '120123', 'Price(cents)': '1999900', 'Id': 'JHk290Xj'}
        data = car_api_implementation.get_car_json_from_data_file()
        self.assertTrue(mock_json, data[0])