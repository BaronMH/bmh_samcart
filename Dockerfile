FROM python:3.9

WORKDIR /bmh_samcart

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt
COPY . .
EXPOSE 105

CMD ["python3.9", "api.py"]