import csv


def get_car_json_from_data_file():
    try:
        file = open("./data/data.csv", "r")
        reader = csv.DictReader(file)
        data = []
        for row in reader:
            data.append(row)
        file.close()
        return data
    except FileNotFoundError as e:
        print("Cannot locate file ", e)



def get_car_json_from_id(car_id):
    data = get_car_json_from_data_file()
    for row in data:
        values = row.values()
        if car_id in values:
            return row
        else:
            continue
    return {}


